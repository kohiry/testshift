import config
from fastapi_users.authentication import (AuthenticationBackend,
                                          CookieTransport, JWTStrategy)

SECRET = config.COOCKIE_SECRET
print(SECRET, 4)


cookie_transport = CookieTransport(cookie_name="user", cookie_max_age=3600)


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=SECRET, lifetime_seconds=3600)


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=cookie_transport,
    get_strategy=get_jwt_strategy,
)
