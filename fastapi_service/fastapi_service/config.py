import os

from dotenv import load_dotenv

load_dotenv()

COOCKIE_SECRET = os.environ.get("COOKIE_SECRET")
