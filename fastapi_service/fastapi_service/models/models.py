from datetime import datetime

from sqlalchemy import (TIMESTAMP, Boolean, Column, Integer, MetaData, String,
                        Table)

metadata = MetaData()

user = Table(
    "user",
    metadata,
    Column("id", Integer, primary_key=False),
    Column("email", String, nullable=False),
    Column("username", String, nullable=False),
    Column("salary", Integer, nullable=False),
    Column("grade", TIMESTAMP, default=datetime.utcnow),
    Column("hashed_password", String, nullable=False),
    Column("is_active", Boolean, default=True, nullable=False),
    Column("is_superuser", Boolean, default=True, nullable=False),
    Column("is_verified", Boolean, default=True, nullable=False),
)
